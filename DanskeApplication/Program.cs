﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanskeApplication
{
    class Program
    {
        private static readonly string FILENAME = "C:\\pyramid.txt";
        static void Main(string[] args)
        {
            using (StreamReader sr = new StreamReader(FILENAME))
            {
                int number = int.Parse(sr.ReadLine());
                List<Node> nodes = new List<Node> { new Node() { Idx = 0, LastNum = number, Sum = number, Path = $"{number}" } };
                
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    List<int> numbers = line.Split(' ').Select(x => int.Parse(x)).ToList();
                    var newNodes = new List<Node>();
                    foreach (var node in nodes)
                    {
                        if (node.LastNum % 2 != numbers[node.Idx] % 2)
                        {
                            newNodes.Add(new Node() { Idx = node.Idx, LastNum = numbers[node.Idx], Sum = node.Sum + numbers[node.Idx], Path = node.Path });
                        }
                        
                        if(node.LastNum % 2 != numbers[node.Idx + 1] % 2)
                        {
                            newNodes.Add(new Node { Idx = node.Idx + 1, LastNum = numbers[node.Idx + 1], Sum = node.Sum + numbers[node.Idx + 1], Path = node.Path });
                        }
                    }
                    nodes = newNodes
                        .GroupBy(
                            x => x.LastNum, 
                            (k, n) => n.Where(x => x.LastNum == k).OrderByDescending(nod => nod.Sum).First()
                            )
                        .Select(x => { x.Path += $", {x.LastNum}"; return x; }).ToList();
                }

                var bestNode = nodes.OrderByDescending(n => n.Sum).First();

                Console.Out.WriteLine($"Max sum: {bestNode.Sum}\nPath: {bestNode.Path}");
                Console.ReadLine();
            }
        }
    }
}

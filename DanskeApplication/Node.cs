﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DanskeApplication
{
    class Node
    {
        public int Idx { get; set; }
        public int Sum { get; set; }
        public int LastNum { get; set; }
        public string Path { get; set; }
    }
}
